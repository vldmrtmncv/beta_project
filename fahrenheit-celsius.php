<?php
$a = '495 353 168 -39 22';
$a = explode(' ', $a);
$otv = [];
for ($i = 0; $i < count($a); $i++) {
    $ref = ($a[$i] - 32) * 5 / 9;
    if ('0.' . explode('.', $ref)[1] >= 0.5 && $ref > 0) {
        $otv[$i] = ceil($ref) . ' ';
    } elseif ('0.' . explode('.', $ref)[1] >= 0.5 && $ref < 0) {
        $otv[$i] = floor($ref) . ' ';
    } elseif ('0.' . explode('.', $ref)[1] < 0.5 && $ref > 0) {
        $otv[$i] = floor($ref) . ' ';
    } else {
        $otv[$i] = ceil($ref) . ' ';
    }
}
echo implode($otv);